const express = require("express");
const cors = require("cors");
const utils = require("./utils");
const app = express();
var router = express.Router();
app.use(cors())

// // task auto store apr in 0h00 UTC
setInterval(() => {
  var date = new Date();
  if (
    (date.getUTCHours() === 0 &&
      date.getUTCMinutes() === 0 &&
      date.getUTCMilliseconds() === 0) ||
    (date.getUTCHours() >= 23 && date.getUTCMinutes() >= 50)
  ) {
    utils.storeApr();
  }
}, 180000); // every 3 minutes

// store apr by call api if nessesary
router.get("/store",  (req, res) => {
  try {
    utils.storeApr();
    res.status(200).json({ code: 200, message: "success" });
  } catch (error) {
    res.status(500).json({ code: 500, message: `error: ${error}` });
  }
});

// get yesterday_apr
router.get("/yesterday_apr", (req, res) => {
  try {
    let yesterday_apr_data = utils.getYesterdayApr();
    res
      .status(200)
      .json({ code: 200, message: "success", data: yesterday_apr_data });
  } catch (error) {
    res.status(500).json({ code: 500, message: `error: ${error}` });
  }
});

app.use(router);

app.listen(5000, () => {
  console.log("apr util server galaxy is running on port 5000.");
});
