
const Contract = require("web3-eth-contract");
const constants = require("../constants")

const {BSC_TEST_NET, VAULT_ABI, ADDRESS_CONTRACT, ADDRESS_ACCOUNT_CONFIG} = constants
Contract.setProvider(BSC_TEST_NET);

const vaultContract = new Contract(
  VAULT_ABI,
  ADDRESS_CONTRACT,
  ADDRESS_ACCOUNT_CONFIG
);

module.exports = {
  vaultContract,
};
