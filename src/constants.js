const fs = require("fs");
const path = require("path");

const RAW_ABI = fs.readFileSync(path.resolve(__dirname, "./abi/vault.json"));

const VAULT_ABI = JSON.parse(RAW_ABI);
const BSC_TEST_NET = "https://data-seed-prebsc-1-s1.binance.org:8545";
const ADDRESS_ACCOUNT = "0xEd18Ffd65B62d33e5Bc9b3b8c3DD36182f3e8761";
const ADDRESS_CONTRACT = "0x17D2fC6Ad999A7063C547c2A8852240179f6bFAE";
const GAS_PRICE = "800000";

const ADDRESS_ACCOUNT_CONFIG = {
  from: ADDRESS_ACCOUNT,
  gasPrice: GAS_PRICE,
};

module.exports = {
  BSC_TEST_NET,
  ADDRESS_ACCOUNT,
  ADDRESS_CONTRACT,
  VAULT_ABI,
  ADDRESS_ACCOUNT_CONFIG,
};
