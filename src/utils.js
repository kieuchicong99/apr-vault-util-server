const https = require("https");
const path = require("path");
const fs = require("fs");
const vault = require("../src/contract/vault");
const { vaultContract } = vault;

// task getPrice
const getPrice = async () => {
  const url = "https://apiv2.galaxydefi.finance/v1/price";
  return new Promise((resolve, reject) => {
    https.get(url, (res) => {
      var body = [];
      res.on("data", (chunk) => {
        body.push(chunk);
      });

      res.on("end", () => {
        try {
          body = JSON.parse(Buffer.concat(body).toString());
        } catch (e) {
          reject(e);
        }
        resolve(body);
      });
    });
  });
};

// task store apr in time 0h UTC
const storeApr = async () => {
  const lpSupply = await vaultContract.methods.lpSupply().call();
  const totalPendingProfit = await vaultContract.methods
    .totalPendingProfitAvailable()
    .call();
  const gPrice = await getPrice();
  const glxPrice = gPrice.prices.GLX;
  console.log("GLX", glxPrice);
  const apr = (totalPendingProfit*glxPrice*100) / lpSupply;
  const apr_json = JSON.stringify(apr);
  console.log("store:", apr_json);
  fs.writeFile(
    path.resolve(__dirname, "./dbdata/yesterday_apr.json"),
    apr_json,
    "utf8",
    (err) => {
      if (err) {
        throw err;
      }
      console.log("store apr success");
    }
  );
  return true;
};

const getYesterdayApr = () => {
  const rawYesterdayApr = fs.readFileSync(
    path.resolve(__dirname, "./dbdata/yesterday_apr.json")
  );
  const yesterdayApr = JSON.parse(rawYesterdayApr);
  console.log(yesterdayApr);
  return yesterdayApr;
};

module.exports = {
  storeApr,
  getYesterdayApr,
};
